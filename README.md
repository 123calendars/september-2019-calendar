Labor Day – September 2
It is very important date for citizens of United States because Labor Day also known as the end of the summer vacations. People plan their holidays two weeks earlier before Labor Day and they combine these day together to get one more holiday. You definitely should note this day on your calendar if you are working in an organization or if you have something to do on this day because most of the public areas are going to be closed.

September Calendar Image Source: https://www.123calendars.com/images/2019/September/September-2019-Calendar.jpg

Labor Day in the United States of America is the official holiday celebrated on the first Monday of September. It honors the American workers' movement and its contributions to the country's power, welfare and laws. Monday is the long weekend of Labor Day weekend known as. It is considered a federal holiday. Since the late 19th century, as trade unions and workers' movements grew, trade unionists proposed to leave one day to celebrate labor. Oregon became the first state to have an official public holiday. When it became an official federal holiday in 1894, thirty states in the United States officially celebrated Labor Day. Canadian Labor Day is also celebrated on the first Monday of September.

However, more than 80 countries in world celebrate this day on May 1 which also known as European [Labor Day](https://en.wikipedia.org/wiki/Labor_Day). If you are working internationally, you should not forget that because in this day, almost every European country has holiday.

Source: https://www.123calendars.com/september-calendar.html